# Sitemap To Atom

A simple web server exposing Atom feeds generated on the fly from sitemaps.

## Usage

First initialize the database
```sh
$> sitemap-to-atom init
Database initialized
```

then start the server
```sh
$> sitemap-to-atom start
Rocket has launched from http://0.0.0.0:8000
```

finally add some sitemap information in the database
```sh
$> sitemap-to-atom add -i 'site-id' -t 'A title to display in the feed' -u 'https://www.examples.com/sitemap.xml'
Sitemap 'site-id' added
```

Done, you can query the webserver to generate a new Atom feed
```sh
$> curl http://localhost:8000/atom/site-id
```

### Cli

The cli provides various commands to manipulate the database, check with `sitemap-to-atom --help`

## Docker

### Image

You can pick the image built via Continuous Integration using the name `registry.gitlab.com/michaellhomme/sitemap-to-atom`, or build your own with the provided `Dockerfile` :
```sh
$> docker build . -t sitemap-to-atom
```

### Volumes

To persist the SQLite database, use a volume mounted on `/app/data`

### Ports

The default HTTP port used by the web server is 8000

### Example usage

Start the web server
```sh
$> docker run --rm -p 8000:8000 -v $PWD/data:/app/data sitemap-to-atom
Rocket has launched from http://0.0.0.0:8000
```

In another shell, using the ID of the created container (get it from `docker ps`)

```sh
# Initialize the database
$> docker exec 77f1a4fa0976 sitemap-to-atom init
Database initialized

# Add a sitemap
$> docker exec 77f1a4fa0976 sitemap-to-atom add -i 'site-id' -t 'A title to display in the feed' -u 'https://www.examples.com/sitemap.xml'
Sitemap 'site-id' added

# Query the server
$> curl http://localhost:8000/atom/site-id
```

## Docker Compose

Create the file `docker-compose.yml`
```yaml
version: "3"
services:
  server:
    image: registry.gitlab.com/michaellhomme/sitemap-to-atom
    restart: unless-stopped
    ports:
      - 8000:8000
    volumes:
      - ./data:/app/data
```

```sh
# Start the server
$> docker compose up -d

# Initialize the database
$> docker compose exec server sitemap-to-atom init
Database initialized

# Add a sitemap
$> docker compose exec server sitemap-to-atom add -i 'site-id' -t 'A title to display in the feed' -u 'https://www.examples.com/sitemap.xml'
Sitemap 'site-id' added

# Query the server
$> curl http://localhost:8000/atom/site-id
```