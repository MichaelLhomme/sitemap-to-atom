# Builder stage
FROM rust AS builder
COPY . /app
WORKDIR /app
RUN CARGO_HOME="/app/.cargo" cargo build --release

# Prod stage
FROM rust AS prod
RUN mkdir /app /app/data
ENV PATH="$PATH:/app"
WORKDIR /app
COPY --from=builder /app/Rocket.toml /app/
COPY --from=builder /app/target/release/sitemap-to-atom /app/
ENTRYPOINT ["/app/sitemap-to-atom"]
CMD ["start"]