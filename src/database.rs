use rocket_db_pools::Database;
use rocket_db_pools::sqlx::sqlite::SqliteRow;
use rocket_db_pools::sqlx::{self, Row, SqliteConnection};

use crate::sitemap::Sitemap;

#[derive(Database)]
#[database("sitemap-to-atom")]
pub struct SitemapDB(sqlx::SqlitePool);

// TODO maybe implement this module using an ORM 

pub async fn create_table(db: &mut SqliteConnection) -> Result<(), String> {
    sqlx::query("CREATE TABLE sitemap_info(id VARCHAR(255) PRIMARY KEY, title VARCHAR(255), url VARCHAR(255))")
        .execute(db).await
        .map_err(|err| err.to_string())
        .and(Ok(()))
    
}

pub async fn find_by_id(db: &mut SqliteConnection, id: &str) -> Result<Sitemap, String> {
    sqlx::query("SELECT * FROM sitemap_info WHERE id = ?")
        .bind(id)
        .fetch_one(db).await
        .map_err(|err| err.to_string())
        .and_then(|row| row_to_sitemap(&row))
}

pub async fn find_all(db: &mut SqliteConnection) -> Result<Vec<Sitemap>, String> {
    sqlx::query("SELECT * FROM sitemap_info")
        .fetch_all(db).await
        .map_err(|err| err.to_string())
        .and_then(|rows| rows_to_sitemap(rows))
}

pub async fn delete_by_id(db: &mut SqliteConnection, id: &str) -> Result<(), String> {
    sqlx::query("DELETE FROM sitemap_info WHERE id = ?")
        .bind(id)
        .execute(db).await
        .map_err(|err| err.to_string())
        .and_then(|res|  match res.rows_affected() {
            1 => Ok(()),
            _ => Err(format!("Sitemap '{}' not found", id))
        })
}

pub async fn insert(db: &mut SqliteConnection, id: &str, title: &str, url: &str) -> Result<(), String> {
    sqlx::query("INSERT INTO sitemap_info VALUES(?, ?, ?)")
        .bind(id)
        .bind(title)
        .bind(url)
        .execute(db).await
        .map_err(|err| err.to_string())
        .and_then(|res|  match res.rows_affected() {
            1 => Ok(()),
            _ => Err(format!("Error inserting sitemap"))
        })
}

pub async fn update(db: &mut SqliteConnection, id: &str, title: &str, url: &str) -> Result<(), String> {
    sqlx::query("UPDATE sitemap_info SET title = ?, url = ? WHERE id = ?")
        .bind(title)
        .bind(url)
        .bind(id)
        .execute(db).await
        .map_err(|err| err.to_string())
        .and_then(|res|  match res.rows_affected() {
            1 => Ok(()),
            _ => Err(format!("Error updating sitemap"))
        })
}

fn rows_to_sitemap(rows: Vec<SqliteRow>) -> Result<Vec<Sitemap>, String> {
    rows
        .iter()
        .map(|row| row_to_sitemap(row))
        .collect::<Vec<Result<_,_>>>()
        .into_iter()
        .collect::<Result<Vec<_>,_>>()
}

// TODO use #[derive(sqlx::FromRow)] instead of manual read
fn row_to_sitemap(row: &SqliteRow) -> Result<Sitemap, String> {
    let id: Result<&str, _> = row.try_get(0);
    if id.is_err() {
        return Err(format!("Error reading id: {:?}", id.err()))
    }

    let title: Result<&str, _> = row.try_get(1);
    if title.is_err() {
        return Err(format!("Error reading title: {:?}", title.err()))
    }

    let url: Result<&str, _> = row.try_get(2);
    if url.is_err() {
        return Err(format!("Error reading url: {:?}", url.err()))
    }
    
    Ok(Sitemap {
        id: id.unwrap().into(),
        title: title.unwrap().into(),
        url: url.unwrap().into()
    })
}