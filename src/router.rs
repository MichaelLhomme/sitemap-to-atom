use log::error;
use std::sync::Arc;
use std::ops::Deref;
use std::time::Duration;
use mini_moka::sync::Cache;
use lazy_static::lazy_static;

use rocket::Route;
use rocket::http::{ContentType, Status};
use rocket_db_pools::Connection;

use crate::atom::construct_feed;
use crate::database::{SitemapDB, find_by_id};
use crate::sitemap::{Sitemap, retrieve_sitemap, parse_sitemap};

pub fn get_routes() -> Vec<Route> {
   routes![get_atom_feed] 
}

#[get("/atom/<id>")]
async fn get_atom_feed(mut db: Connection<SitemapDB>, id: &str) -> Result<(ContentType, String), Status> {
    let res = find_by_id(&mut **db, id).await;
    match res {
        Err(err) => {
            error!("Error fetching sitemap '{}': {}", id, err);
            return Err(Status::NotFound);
        },

        Ok(sitemap) => construct_feed_for_sitemap(&sitemap)
            .await
            .map_err(|err| {
                error!("Error generating feed for sitemap '{}': {}", sitemap.id, err);
                Status::InternalServerError
            })
            .map(|xml| (ContentType::XML, xml))
    }
}

lazy_static! {
    static ref CACHE: Cache<String, Arc<String>> = Cache::builder()
        .time_to_live(Duration::from_secs(30 * 60))
        .build();
}

async fn construct_feed_for_sitemap(sitemap: &Sitemap) -> Result<String, String> {
    get_sitemap(&sitemap.url)
        .await
        .and_then(|text| parse_sitemap(&text))
        .and_then(|links| construct_feed(&sitemap, links))
        .map(|feed| feed.to_string())
}

async fn get_sitemap(url: &str) -> Result<String, String> {
    match CACHE.get(&url.to_string()) {
        Some(content) => Ok(content.deref().to_owned()),
        None => retrieve_sitemap(url)
            .await
            .map_err(|err| format!("Error fetching sitemap: {}", err))
            .and_then(|content| {
                CACHE.insert(url.to_string(), Arc::new(content.clone()));
                Ok(content)                
            })
    }
}

// TODO try to create an endpoint for debug taking a sitemap.xml as multipart file
/*

#[derive(FromForm)]
struct Upload<'r> {
    file: rocket::fs::TempFile<'r>
}

#[post("/atom/<id>", data = "<upload>")]
async fn get_atom_feed_from_file(
    mut db: Connection<SitemapDB>,
    id: &str,
    upload: rocket::Data<'_>
) -> Result<(ContentType, String), String> {
    let res = find_by_id(&mut **db, id)
        .await
        .map(|sitemap| async move {
            // TODO read upload
            // TODO better to stream directly instead of using a tempfile ?
            let content = "";
            parse_sitemap(content.into())
                .and_then(|links| construct_feed(&sitemap, links))
                .map(|f| (ContentType::XML,f.to_string()))
            }
        );

        match res {
            Err(err) => Err(err),
            Ok(future) => future.await
        }
}
*/