use log::info;

use url::Url;
use uuid::Uuid;
use chrono::{DateTime, NaiveDate, NaiveTime, ParseError, Utc };
use atom_syndication::{Feed, Entry, FeedBuilder, LinkBuilder, EntryBuilder};

use crate::sitemap::{Sitemap, SitemapUrl};

pub fn construct_feed(sitemap: &Sitemap, links: Vec<SitemapUrl>) -> Result<Feed, String> {
    info!("Constructing feed from sitemap {} ({} links found)", sitemap.id, links.len());
    
    links
        .iter()
        .map(|u| construct_entry_from_sitemap_url(u))
        .collect::<Vec<Result<_, _>>>()
        .into_iter()
        .collect::<Result<Vec<_>, _>>()
        .map(|entries| FeedBuilder::default()
            .title(sitemap.title.clone())
            .updated(extract_max_date(&entries))
            .entries(entries)
            .build()
        )
}

fn construct_entry_from_sitemap_url(su: &SitemapUrl) -> Result<Entry, String> {
    let date = parse_date(&su.date);
    if date.is_err() {
        return Err(format!("Error parsing date '{}': {}", su.date, date.unwrap_err()));
    }

    let title = extract_title(&su.url);
    if title.is_err() {
        return Err(format!("Error parsing title from url '{}': {}", su.url, title.unwrap_err()));
    }

    let e = EntryBuilder::default()
        .id(generate_unique_id(&su.url))
        .title(title.unwrap())
        .updated(date.unwrap())
        .link(LinkBuilder::default()
              .href(su.url.clone())
              .build()
        )
        .build();

    Ok(e)
}

fn generate_unique_id(url: &str) -> String {
    Uuid::new_v5(&Uuid::NAMESPACE_URL, url.as_bytes()).to_string()
}

fn extract_title(url: &str) -> Result<String, String> {
    Url::parse(url)
        .map_err(|err| format!("Error parsing url '{}': {}", url, err))
        .and_then(|u| u
            .path_segments()
            .map(|segments| segments.filter(|p| !p.is_empty()))
            .and_then(|segments| segments.last())
            .map(|s| String::from(s))
            .ok_or(String::from("No path segments"))
            .or(u
                .host_str()
                .map(|s| String::from(s))
                .ok_or(String::from("No domain for fallback"))
            )
        )
}

fn parse_date(str: &str) -> Result<DateTime<Utc>, ParseError> {
    DateTime::parse_from_rfc3339(&str)
        .map(|date| date.with_timezone(&Utc))
        .or_else(|_| NaiveDate::parse_from_str(&str, "%Y-%m-%d")
                 .map(|date| date
                      .and_time(NaiveTime::MIN)
                      .and_utc()
                )
        )
}

fn extract_max_date(links: &Vec<Entry>) -> DateTime<Utc> {
    links
        .iter()
        .map(|entry| entry.updated.with_timezone(&Utc))
        .fold(DateTime::<Utc>::MIN_UTC, |a,b| a.max(b))
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_fails_to_parse_invalid_date() {
        let res = parse_date("hello");
        assert_eq!(res.is_err(), true);
    }

    #[test]
    fn it_parses_complete_date() {
        let res = parse_date("2023-11-14T15:52:25+00:00");
        assert_eq!(res.is_ok(), true);
    }

    #[test]
    fn it_parses_simple_date() {
        let res = parse_date("2023-11-14");
        assert_eq!(res.is_ok(), true);
    }

    #[test]
    fn it_generate_idempotent_id() {
        let id1 = generate_unique_id("https://www.example.com");
        let id2 = generate_unique_id("https://www.example.com");
        assert_eq!(id1, id2)
    }

    #[test]
    fn it_fails_to_extract_title_from_invalid_url() {
        let res = extract_title("hello");
        assert_eq!(res.is_err(), true);
    }

    #[test]
    fn it_extracts_title_from_last_path() {
        let res = extract_title("https://www.example.com/lastpath");
        assert_eq!(res, Ok(String::from("lastpath")));
    }

    #[test]
    fn it_extracts_title_from_last_non_empty_path() {
        let res = extract_title("https://www.example.com/lastpath/");
        assert_eq!(res, Ok(String::from("lastpath")));
    }

    #[test]
    fn it_extracts_title_from_domain_as_fallback() {
        let res = extract_title("https://www.example.com/");
        assert_eq!(res, Ok(String::from("www.example.com")));
    }

}

