use colored::Colorize;

#[macro_use] extern crate rocket;
use log::{Level, LevelFilter};
use rocket::{Rocket, Ignite};
use rocket::futures::TryFutureExt;

use rocket_db_pools::sqlx::Acquire;
use rocket_db_pools::{Database, Pool};

pub mod cli;
pub mod atom;
pub mod router;
pub mod sitemap;
pub mod database;

use router::get_routes;
use database::SitemapDB;
use cli::SitemapToAtomCli;

#[rocket::main]
async fn main() -> Result<(), String> {
    match <SitemapToAtomCli as clap::Parser>::parse() {
        SitemapToAtomCli::Start => start_rocket_web()
            .await
            .map_err(|err| format!("Error executing rocket {}", err))
            .and(Ok(())),
            
        args => start_rocket_cli(&args).await
    }
}

async fn start_rocket_web() -> Result<Rocket<Ignite>, rocket::Error> {
    init_logger(LevelFilter::Info);
    
    rocket::build()
        .attach(SitemapDB::init())
        .mount("/", get_routes())
        .launch()
        .await
}

async fn start_rocket_cli(args: &SitemapToAtomCli) -> Result<(), String> {
    init_logger(LevelFilter::Off);
    
    let rocket = rocket::build()
        .attach(SitemapDB::init())
        .ignite()
        .await
        .unwrap();

    let db = SitemapDB::fetch(&rocket)
        .ok_or("Unable to access database".to_string())
        .unwrap();

    let mut pool = db.get().await.unwrap();
    pool
        .acquire()
        .map_err(|err| format!("Error opening connection: {}", err))
        .and_then(|conn| cli::process_args(conn, &args))
        .await
}

fn init_logger(base_level: LevelFilter) -> () {
    fern::Dispatch::new()
        .format(|out, message, record| {
            let lvl = record.level().as_str();
            let prefix = match record.level() {
                Level::Error => lvl.red().bold(),
                Level::Warn => lvl.yellow().bold(),
                Level::Info => lvl.blue().bold(),
                Level::Debug => lvl.green().bold(),
                Level::Trace => lvl.bold()
            };
            
            out.finish(format_args!(
                "{} {} {} {}",
                humantime::format_rfc3339_seconds(std::time::SystemTime::now()),
                prefix,
                record.target(),
                message
            ))
        })
        .chain(
            fern::Dispatch::new()
                .level(base_level)
                .level_for("sitemap_to_atom", LevelFilter::Debug)
                .chain(std::io::stdout())
        )
        .apply()
        .unwrap();
}

