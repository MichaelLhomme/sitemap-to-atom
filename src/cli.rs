use clap::command;
use rocket_db_pools::sqlx::SqliteConnection;

use crate::database;
use crate::sitemap::Sitemap;

#[derive(clap::Parser)]
#[command(name = "sitemap-to-atom")]
#[command(bin_name = "sitemap-to-atom")]
pub enum SitemapToAtomCli {
    /// Start the web server
    Start,
    
    /// Create the initial database structure
    Init,

    /// List the known sitemaps
    List,

    /// Show information about a given sitemap
    Show(IdArgs),

    /// Add a new sitemap
    Add(FullArgs),

    /// Update a sitemap identified by its ID
    Update(FullArgs),
    
    /// Delete a sitemap
    Delete(IdArgs),
}

#[derive(Debug, clap::Args)]
#[command(author, version, about, long_about = None)]
pub struct IdArgs {
    #[arg(short, long)]
    id: String,
}

#[derive(Debug, clap::Args)]
#[command(author, version, about, long_about = None)]
pub struct FullArgs {
    #[arg(short, long)]
    id: String,

    #[arg(short, long)]
    title: String,

    #[arg(short, long)]
    url: String,
}

pub async fn process_args(db: &mut SqliteConnection, args: &SitemapToAtomCli) -> Result<(), String> {
    match args {
        SitemapToAtomCli::Start => Err("Should not be happening".to_string()),
        SitemapToAtomCli::Init => init(db).await,
        SitemapToAtomCli::List => list(db).await,
        SitemapToAtomCli::Show(args) => show(db, args).await,
        SitemapToAtomCli::Delete(args) => delete(db, args).await,
        SitemapToAtomCli::Add(args) => add(db, args).await,
        SitemapToAtomCli::Update(args) => update(db, args).await,
    }
}

async fn init(db: &mut SqliteConnection) -> Result<(), String> {
    database::create_table(db)
        .await
        .and_then(|()| {
            println!("Database initialized");
            Ok(())
        })
}

async fn list(db: &mut SqliteConnection) -> Result<(), String> {
    database::find_all(db)
        .await
        .map(|vec| match vec.len() {
            0 => println!("No sitemap found !"),
            _ => vec
                .iter()
                .for_each(|sitemap| print_sitemap(sitemap))
        })
}

async fn add(db: &mut SqliteConnection, args: &FullArgs) -> Result<(), String> {
    database::insert(db, &args.id, &args.title, &args.url)
        .await
        .and_then(|()| {
            println!("Sitemap '{}' added", args.id);
            Ok(())
        })
}

async fn show(db: &mut SqliteConnection, args: &IdArgs) -> Result<(), String> {
    database::find_by_id(db , &args.id)
        .await
        .map(|sitemap| print_sitemap(&sitemap))
}

async fn update(db: &mut SqliteConnection, args: &FullArgs) -> Result<(), String> {
    database::update(db, &args.id, &args.title, &args.url)
        .await
        .and_then(|()| {
            println!("Sitemap '{}' updated", args.id);
            Ok(())
        })
}

async fn delete(db: &mut SqliteConnection, args: &IdArgs) -> Result<(), String> {
    database::delete_by_id(db, &args.id)
        .await
        .and_then(|()| {
            println!("Sitemap '{}' deleted", args.id);
            Ok(())
        })
}

fn print_sitemap(sitemap: &Sitemap) -> () {
    println!("{}: '{}' at {}", sitemap.id, sitemap.title, sitemap.url)
}