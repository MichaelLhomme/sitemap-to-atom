use log::{debug, trace};

use quick_xml::events::Event;
use quick_xml::reader::Reader;

#[derive(Debug)]
pub struct Sitemap {
    pub id: String,
    pub url: String,
    pub title: String,
}

#[derive(Debug)]
pub struct SitemapUrl {
    pub url: String,
    pub date: String
}

pub async fn retrieve_sitemap(sitemap_url: &str) -> Result<String, reqwest::Error> {
    debug!("Fetching sitemap at '{}'...", sitemap_url);
    
    let res = reqwest::Client::new()
        .get(sitemap_url)
        .header(reqwest::header::USER_AGENT, "curl")
        .send()
        .await
        .and_then(|res| res.error_for_status())
        .map(|res| res.text());

    match res {
        Err(err) => Err(err.into()),
        Ok(text) => text.await
    }
}

pub fn parse_sitemap(str: &str) -> Result<Vec<SitemapUrl>, String> {
    debug!("Parsing sitemap...");

    let mut reader = Reader::from_str(str);
    reader.trim_text(true);

    let mut res = Vec::new();

    loop {
        match reader.read_event() {
            Err(e) => return Err(format!("Error at position {}: {:?}", reader.buffer_position(), e)),
            Ok(Event::Eof) => break,
            Ok(Event::Start(e)) => {
                match e.name().as_ref() {
                    b"url" => match parse_url_node(&mut reader) {
                        Err(err) => return Err(format!("{}", err)),
                        Ok(url) => res.push(url)
                    },
                    _ => ()
                }
            },
            _ => (),
        }
    }

    return Ok(res);
}

fn parse_url_node(reader: &mut Reader<&[u8]>) -> Result<SitemapUrl, String> {
    trace!("Parsing url node");

    let mut url = String::new();
    let mut date = String::new();

    loop {
        match reader.read_event() {
            Ok(Event::Start(e)) => match e.name().as_ref() {
                b"loc" => match parse_text_node(reader, "loc") {
                    Err(err) => return Err(err),
                    Ok(txt) => url = txt
                },
                b"lastmod" => match parse_text_node(reader, "lastmod") {
                    Err(err) => return Err(err),
                    Ok(txt) => date = txt
                },
                _ => match consume_node(reader, get_node_name(e.name()).as_str()) {
                    Err(err) => return Err(err),
                    Ok(_) => ()
                }
            },
            Ok(Event::End(e)) => match e.name().as_ref() {
                b"url" => {
                    return Ok(SitemapUrl {
                        url: url.clone(),
                        date:date.clone()
                    });
                },
                _ => return Err(format!("Unexpected node end {}, was looking for url", get_node_name(e.name())))
            },
            e => return Err(format!("Expecting url content but received other thing {:?}", e))
        }
    }
}

fn get_node_name(name: quick_xml::name::QName) -> String {
    String::from_utf8(name.as_ref().to_vec()).unwrap()
}

fn consume_node(reader: &mut Reader<&[u8]>, node: &str) -> Result<(), String> {
    trace!("Consuming node '{}'", node);

    loop {
        match reader.read_event() {
            Err(err) => return Err(format!("Received error when consuming node {}: {}", node, err)),
            Ok(Event::Eof) => return Err(format!("Reached eof while looking for closing tag {}", node)),
            Ok(Event::End(e)) => {
                let closing_tag_name = get_node_name(e.name());
                match node.eq(&closing_tag_name) {
                    true => return Ok(()),
                    _ => ()
                }
            },
            _ => ()
        }
    }
}

fn parse_text_node(reader: &mut Reader<&[u8]>, node: &str) -> Result<String, String> {
    trace!("Parsing text node '{}'", node);

    parse_text(reader)
        .and_then(|txt| expect_node_end(reader, node)
            .map(|_| txt)
        )
}

fn expect_node_end(reader: &mut Reader<&[u8]>, node: &str) -> Result<(), String> {
    trace!("Expecting node end '{}'", node);
    
    match reader.read_event() {
        Ok(Event::End(e)) => {
            let closing_tag_name = get_node_name(e.name());
            match node.eq(&closing_tag_name) {
                true => Ok(()),
                false => Err(format!("Unexpected closing tag {}, was looking for {}", closing_tag_name, node))
            }
        }
        e => Err(format!("Expecting closing tag for {} but received other thing {:?}", node, e))
    }
}

fn parse_text(reader: &mut Reader<&[u8]>) -> Result<String, String> {
    match reader.read_event() {
        Ok(Event::Text(e)) => Ok(e.unescape().unwrap().into_owned()),
        e => Err(format!("Expecting text but received other thing {:?}", e))
    }
}
